<!DOCTYPE html>
<html>

<head>
    <title>Cours PHP / MySQL</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="cours.css">
</head>

<body>
    <h1>Nintendo 64 Console Games</h1>
    <?php
	//connexion à ma base MSQL
	$username = 'root';
	$password = '';
	//connexion via PDO
	try {
		$bdd = new PDO('mysql:host=localhost;dbname=test; port=3308', $username, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		//$quer = $bdd->query('SELECT * FROM jeux_video');
	    //$reponse = $quer->fetch(PDO::FETCH_ASSOC);
	//var_dump($reponse);
	} 
	catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br/>";
		die();
	}
	$reponse = $bdd->query('SELECT * FROM jeux_video');
	while ($donnees = $reponse->fetch())
{
?>
    <p>
        <strong>Jeu</strong> : <?php echo $donnees['nom']; ?><br />
        Le possesseur de ce jeu est : <?php echo $donnees['possesseur']; ?>, et il le vend à
        <?php echo $donnees['prix']; ?> euros !<br />
        Ce jeu fonctionne sur <?php echo $donnees['console']; ?> et on peut y jouer à
        <?php echo $donnees['nbre_joueurs_max']; ?> au maximum<br />
        <?php echo $donnees['possesseur']; ?> a laissé ces commentaires sur <?php echo $donnees['nom']; ?> :
        <em><?php echo $donnees['commentaires']; ?></em>
    </p>
    <?php
}
    //requête vers ma table "jeux_video" dans ma base "test"
	//$reponse = $bdd->query('SELECT * FROM jeux');
	// //boucle fetch
	// while($donnees = $reponse->fetch()){
	//  afficher les colonnes id nom console possesseur prix commentaires
	// 	echo $donnees['id']."</br>";
	// 	echo $donnees['nom']."</br>";
	// 	echo $donnees['console']."</br>";
	// 	echo $donnees['possesseur']."</br>";
	// 	echo $donnees['prix']."</br>";
	// 	echo $donnees['commentaires']."</br>";
	// 	echo "<hr>";
	//}
	// Termine le traitement de la requête
	//$reponse->closeCursor(); 
?>
</body>

</html>